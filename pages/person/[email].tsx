import { useRouter } from 'next/router';
import React, { useEffect, useRef, useState } from 'react';
import { Button, DatePicker, PageHeader, Descriptions, Input, message } from 'antd';
import moment from 'moment';
import styled from 'styled-components';

import { withContextInitialized } from '../../components/hoc';
import CompanyCard from '../../components/molecules/CompanyCard';
import GenericList from '../../components/organisms/GenericList';
import OverlaySpinner from '../../components/molecules/OverlaySpinner';
import { usePersonInformation } from '../../components/hooks/usePersonInformation';

import { Company, Person } from '../../constants/types';
import { ResponsiveListCard } from '../../constants';
import { MaskedInput } from 'antd-mask-input';

const EditionContainer = styled.div`
  display: grid;
  gap: 20px;
  max-width: 300px;
`;

const PersonDetail = () => {
  const router = useRouter();
  const { load, loading, save, data } = usePersonInformation(
    router.query?.email as string,
    true
  );

  const [enableEdition, setEnableEdition] = useState<Person | undefined>(undefined);

  useEffect(() => {
    load();
  }, []);

  if (loading) {
    return <OverlaySpinner title={`Loading ${router.query?.email} information`} />;
  }

  if (!data) {
    message.error("The user doesn't exist redirecting back...", 2, () =>
      router.push('/home')
    );
    return <></>;
  }

  const handleSave = () => {
    save(enableEdition);
    setEnableEdition(undefined);
  };

  console.log({ data, enableEdition })

  return (
    <>
      <PageHeader
        onBack={router.back}
        title="Person"
        subTitle="Profile"
        extra={[
          <Button
            style={{ padding: 0, margin: 0 }}
            type="link"
            href={data.website}
            target="_blank"
            rel="noopener noreferrer"
          >
            Visit website
          </Button>,
          enableEdition ? (
            <Button type="default" onClick={() => handleSave()}>
              Save
            </Button>
          ) : (
            <Button type="default" onClick={() => setEnableEdition(data)}>
              Edit
            </Button>
          ),
        ]}
      >
        {data && (
          enableEdition ? (
            <EditionContainer>
              <Input
                defaultValue={data.name}
                onChange={event => setEnableEdition({ ...enableEdition, name: event.target.value })}
                value={enableEdition.name}
              />
              <Input
                defaultValue={data.gender}
                onChange={event => setEnableEdition({ ...enableEdition, gender: event.target.value })}
                value={enableEdition.gender}
              />
              <MaskedInput
                defaultValue={data.phone}
                mask='+0000000000000'
                onChange={event => setEnableEdition({ ...enableEdition, phone: event.target.value })}
                value={enableEdition.phone}
              />
              <DatePicker
                defaultPickerValue={moment(data.birthday)}
                onChange={date => setEnableEdition({ ...enableEdition, birthday: date ? date.format('YYYY-MM-DD') : data.birthday })}
                value={moment(enableEdition.birthday)}
              />
            </EditionContainer>
          ) : (
            <Descriptions size="small" column={1}>
              <Descriptions.Item label="Name">{data.name}</Descriptions.Item>
              <Descriptions.Item label="Gender">{data.gender}</Descriptions.Item>
              <Descriptions.Item label="Phone">{data.phone}</Descriptions.Item>
              <Descriptions.Item label="Birthday">{data.birthday}</Descriptions.Item>
            </Descriptions>
          )
        )}
        <GenericList<Company>
          loading={loading}
          extra={ResponsiveListCard}
          data={data && data.companyHistory}
          ItemRenderer={({ item }: any) => <CompanyCard item={item} />}
          handleLoadMore={() => { }}
          hasMore={false}
        />
      </PageHeader>
    </>
  );
};

export default withContextInitialized(PersonDetail);
